FROM node:12.13.0-slim

LABEL maintainer="Jonathan Robic <hello@jonathanrobic.fr>"
LABEL version="1.0.0"

ENV PROJECT_DIR=/app
ENV PORT=80

# Create app directory
WORKDIR ${PROJECT_DIR}

# Copy workspace config
COPY ./package.json .
COPY ./yarn.lock .
COPY ./lerna.json .
COPY ./packages/common/package.json ./packages/common/
COPY ./packages/server/package.json ./packages/server/

# Install dependencies for packages
RUN yarn workspace @catsmash/common install
RUN yarn workspace @catsmash/server install

# Copy packages
COPY ./packages/common ./packages/common
COPY ./packages/server ./packages/server

# Build server
RUN cd packages/common && yarn build
RUN cd packages/server && yarn build

# Run the app
EXPOSE ${PORT}

WORKDIR ${PROJECT_DIR}/packages/server

ENV NODE_ENV production

CMD ["yarn", "start"]
