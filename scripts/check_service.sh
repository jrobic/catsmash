#!/usr/bin/env bash

nbSuccess=5
waitTime=10

version=$(cat packages/server/package.json | jq -r ".version")
name=$(cat packages/server/package.json | jq -r ".name")

success=0
url="https://catsmash-api.jonathanrobic.com/version"

for i in {1..60}
do
  echo "Checking $i times"
  result=$(curl -s ${url})

  remoteVersion=$(echo $result | jq -r ".version")
  remoteName=$(echo $result | jq -r ".name")

  echo "${name}[${version}] <> ${remoteName}[${remoteVersion}]"

  if [[ "$name" == "$remoteName" && "$version" == "$remoteVersion" ]]; then
    success=$((success+1))
    exit
  else
    success=0
  fi
  sleep $waitTime
done

exit 2
