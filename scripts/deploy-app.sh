#!/usr/bin/env bash

set -e

bucket=catsmash-app
distribution=ETNIBV7CC5F3Z

cd packages/web && yarn build

aws s3 sync build s3://${bucket} --delete
aws cloudfront create-invalidation --distribution-id ${distribution} --paths "/*"
