#!/usr/bin/env bash

stackName="catsmash-api"
region="eu-west-2"

cluster=$(aws cloudformation describe-stacks --stack-name ${stackName} --region ${region} | jq -r ".Stacks | .[0] .Outputs | map(select(.OutputKey | contains (\"Cluster\"))) | .[] | .OutputValue")
service=$(aws ecs list-services --cluster ${cluster} --region ${region} | jq -r ".serviceArns | map(select(contains(\"catsmash-api-service\"))) | .[0]")
echo "Updating AWS service..."

aws ecs update-service --cluster ${cluster} --service ${service} --region ${region} --force-new-deployment
echo "Done"
