#!/usr/bin/env bash

set -e

rev=$(git rev-parse --short HEAD)
registry=758577872946.dkr.ecr.eu-west-2.amazonaws.com
service="catsmash-api"

image=${registry}/${service}

$(aws ecr get-login --no-include-email --region eu-west-2)

docker pull ${image}:latest || true
docker build --pull --cache-from ${image}:latest --tag ${image}:${rev} --tag ${image}:${latest} .




tag=${registry}/${service}

docker push ${image}:${rev}
docker push ${image}:latest
