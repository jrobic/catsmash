[![pipeline status](https://gitlab.com/jrobic/catsmash/badges/master/pipeline.svg)](https://gitlab.com/jrobic/catsmash/commits/master)

# Catsmash App

[Demo](https://catsmash.jonathanrobic.com)

## Requirements

- node 12.13.0
- yarn
- docker
- docker-compose

## Setup

### Yarn

Install all packages depencies

`yarn` or `yarn install`

### Docker

Start **prisma** and **postgres** service
`yarn dev:dc`

## Development

### Server

Start server

`yarn dev:server` or `cd packages/server && yarn dev`

### Web

Start web app

`yarn dev:web` or `cd packages/web && yarn dev`

## Others

_Coming Soon_
