import { sayHello } from "./sayHello";

test("should return string with default name", () => {
  expect(sayHello()).toEqual("Hello, World");
});

test("should return string", () => {
  expect(sayHello("John")).toEqual("Hello, John");
});
