# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.4.0](https://gitlab.com/jrobic/catsmash/compare/v0.3.0...v0.4.0) (2020-02-05)

**Note:** Version bump only for package @catsmash/common





# [0.3.0](https://gitlab.com/jrobic/catsmash/compare/v0.2.0...v0.3.0) (2020-02-04)

**Note:** Version bump only for package @catsmash/common





# [0.2.0](https://gitlab.com/jrobic/catsmash/compare/v0.1.0...v0.2.0) (2020-02-04)

**Note:** Version bump only for package @catsmash/common





# [0.1.0](https://gitlab.com/jrobic/catsmash/compare/v0.0.2...v0.1.0) (2020-02-02)

**Note:** Version bump only for package @catsmash/common





## 0.0.2 (2020-02-02)

**Note:** Version bump only for package @catsmash/common
