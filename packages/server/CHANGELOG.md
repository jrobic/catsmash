# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.4.0](https://gitlab.com/jrobic/catsmash/compare/v0.3.0...v0.4.0) (2020-02-05)

**Note:** Version bump only for package @catsmash/server





# [0.3.0](https://gitlab.com/jrobic/catsmash/compare/v0.2.0...v0.3.0) (2020-02-04)

**Note:** Version bump only for package @catsmash/server





# [0.2.0](https://gitlab.com/jrobic/catsmash/compare/v0.1.0...v0.2.0) (2020-02-04)


### Bug Fixes

* **prisma:** use mutli staged correctly ([5eb7add](https://gitlab.com/jrobic/catsmash/commit/5eb7add2fd0a54636c19c416c57df846ff74af3a))


### Features

* **rating:** add rank ([3ae6b08](https://gitlab.com/jrobic/catsmash/commit/3ae6b08ecb1a6b66a7d40a86fc270cb25b1cf78b))
* **rating:** rating system w/ generate random cat ([6f10077](https://gitlab.com/jrobic/catsmash/commit/6f10077cd51a28218b8e12c9a4c996be6ff767c5))





# [0.1.0](https://gitlab.com/jrobic/catsmash/compare/v0.0.2...v0.1.0) (2020-02-02)


### Features

* **server:** initialize server w/ graphql, prisma ([52defd8](https://gitlab.com/jrobic/catsmash/commit/52defd86b41de03580e36ecbb5f634e2e2beb338))
