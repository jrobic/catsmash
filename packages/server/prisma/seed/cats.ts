import axios from "axios";
import { Cat, prisma } from "../../src/generated/prisma-client";

interface CatImage {
  url: string;
  id: string;
}

async function createOrUpdateCat(catImage: CatImage): Promise<Cat> {
  return prisma.upsertCat({
    where: { image: catImage.url },
    update: {},
    create: { image: catImage.url },
  });
}

export async function loadCats(url: string): Promise<Cat[]> {
  try {
    const { data } = (await axios.get(url)) as { data: { images: CatImage[] } };
    return Promise.all(data.images.map(createOrUpdateCat));
  } catch (err) {
    console.log(err);
  }

  return [];
}
