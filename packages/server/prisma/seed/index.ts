import { loadCats } from "./cats";

const NODE_ENV = process.env.NODE_ENV || "development";

async function main(): Promise<void> {
  if (NODE_ENV === "development") {
    const cats = await loadCats("https://latelier.co/data/cats.json");
    console.log(`[SEED] ${cats.length} was loaded successfuly!`);
  }

  await Promise.resolve();
}

main();
