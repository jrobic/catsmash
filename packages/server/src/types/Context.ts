import Express from "express";
import { Prisma } from "../generated/prisma-client";

export interface MyContext {
  req: Express.Request;
  res: Express.Response;
  url: string;
  prisma: Prisma;
}
