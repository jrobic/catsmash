export const rawQuery = (sql: string): string =>
  `
  ${sql}
`
    .replace(/"/g, '\\"')
    .replace(/\n/g, " ")
    .replace(/\s+/g, " ");
