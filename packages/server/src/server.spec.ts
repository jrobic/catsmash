import { createServer, ServerTest } from "./tests-utils/common";
import { App } from "./types/Server";

describe("> Server", (): void => {
  let server: ServerTest;
  let app: App;

  beforeAll(
    async (): Promise<void> => {
      ({ server, app } = await createServer());
    },
  );

  afterAll(
    async (): Promise<void> => {
      await app.close();
    },
  );

  test("should return api version", async (): Promise<void> => {
    await server
      .get("/version")
      .expect(({ body }: any): void => {
        expect(body.name).toBeDefined();
        expect(body.version).toBeDefined();
      })
      .expect(200);
  });
});
