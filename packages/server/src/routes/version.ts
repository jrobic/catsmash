import { Request, Response } from "express";
import { Version, name, version } from "../utils/version";

export default (_req: Request, res: Response): Response => {
  const apiVersion: Version = { version, name };
  return res.send(apiVersion);
};
