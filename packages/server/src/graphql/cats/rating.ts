/**
 * @see https://en.wikipedia.org/wiki/Elo_rating_system#Most_accurate_K-factor
 * ICC seems to adopt a global K=32 except when playing against provisionally rated players
 */
const KFACTOR = 32;

// const CATEGORIES = {
//   0: [2400, Infinity],
//   1: [2200, 2399],
//   2: [2000, 2199],
//   3: [1800, 1999],
//   4: [1600, 1799],
//   5: [1400, 1599],
//   6: [1200, 1399],
//   7: [1000, 1199],
//   8: [800, 999],
//   9: [600, 799],
//   10: [400, 599],
//   11: [200, 399],
//   12: [100, 199],
// };

export const expectedScore = (Rb: number, Ra: number): number =>
  // eslint-disable-next-line no-restricted-properties
  parseFloat((1 / (1 + Math.pow(10, (Rb - Ra) / 400))).toFixed(4));

export const winnerRating = (score: number, expected: number): number =>
  score + KFACTOR * (1 - expected);

export const loserRating = (score: number, expected: number): number =>
  score + KFACTOR * (0 - expected);

export const rank = (score: number, wins: number, losses: number): number => {
  if (wins === 0 || losses === 0) {
    return Math.round(score);
  }
  return Math.round(score / (1 + losses / wins));
};

export const rate = (Rw: number, Rl: number): { Rw: number; Rl: number } => {
  // Rw: Rating of Winner
  // Rl: Rating of Loser
  const El = expectedScore(Rw, Rl);
  const Ew = expectedScore(Rl, Rw);

  const nRw = winnerRating(Rw, Ew);
  const nRl = loserRating(Rl, El);

  return {
    Rw: nRw < 100 ? 100 : nRw,
    Rl: nRl < 100 ? 100 : nRl,
  };
};
