import { rawQuery } from "../../utils/rawQuery";
import { MyContext } from "../../types/Context";
import {
  CatWhereInput,
  CatOrderByInput,
  Int,
  AggregateCat,
  CatConnection,
  Cat,
  CatWhereUniqueInput,
} from "../../generated/prisma-client";
import { RateInput } from "../../types/rating";
import { rate, rank } from "./rating";

interface CatConnectionArgs {
  where: CatWhereInput;
  orderBy: CatOrderByInput;
  skip: Int;
  after: string;
  before: string;
  first: Int;
  last: Int;
}

interface MyCatConnection extends CatConnection {
  aggregate: AggregateCat;
}

export default {
  Query: {
    async catsConnection<Root>(
      _root: Root,
      args: CatConnectionArgs,
      ctx: MyContext,
    ): Promise<MyCatConnection> {
      const catsList = await ctx.prisma.catsConnection({
        ...args,
      });

      const count = await ctx.prisma
        .catsConnection()
        .aggregate()
        .count();

      return {
        ...catsList,
        aggregate: {
          count,
        },
      };
    },
    async cat<Root>(
      _root: Root,
      args: { where: CatWhereUniqueInput },
      ctx: MyContext,
    ): Promise<Cat> {
      const cat = await ctx.prisma.cat(args.where);

      if (!cat) {
        throw new Error("CAT_NOT_FOUND");
      }

      return cat;
    },
    async random<Root, Args>(_root: Root, _args: Args, ctx: MyContext): Promise<Cat[]> {
      const namespace = process.env.PRISMA_ENDPOINT?.split("/")
        .splice(-2)
        .join("$") as string;

      const query = rawQuery(`SELECT * FROM "${namespace}"."Cat" ORDER BY random() LIMIT 2`);

      const { executeRaw } = await ctx.prisma.$graphql(`
        mutation {
          executeRaw(query: "${query}")
        }
      `);

      return executeRaw;
    },
  },
  Mutation: {
    async rate<Root>(_root: Root, args: RateInput, ctx: MyContext): Promise<Cat[]> {
      let winner = await ctx.prisma.cat({ id: args.winner });
      let loser = await ctx.prisma.cat({ id: args.loser });

      if (!winner || !loser) {
        throw new Error("CAT_NOT_FOUND");
      }

      // score
      const wins = winner.wins + 1;
      const losses = loser.losses + 1;

      // expected score
      const { Rw, Rl } = rate(winner.rate, loser.rate);

      // rank
      const winnerRank = rank(Rw, wins, winner.losses);
      const loserRank = rank(Rl, loser.wins, losses);

      winner = await ctx.prisma.updateCat({
        data: { rate: Rw, rank: winnerRank, wins },
        where: { id: winner.id },
      });
      loser = await ctx.prisma.updateCat({
        data: { rate: Rl, rank: loserRank, losses },
        where: { id: loser.id },
      });

      return [winner, loser];
    },
  },
};
