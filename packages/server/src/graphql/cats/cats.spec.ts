// eslint-disable-next-line import/no-extraneous-dependencies
import request from "supertest";
import { createServer, ServerTest } from "../../tests-utils/common";
import { App } from "../../types/Server";
import { Cat } from "../../generated/prisma-client";

const catQuery = /* GraphQL */ `
  query catQuery($where: CatWhereUniqueInput!) {
    cat(where: $where) {
      id
      image
      rate
      wins
      losses
    }
  }
`;

const randomQuery = /* GraphQL */ `
  query randomQuery {
    random {
      id
      image
      rate
      wins
      losses
    }
  }
`;

const catsQuery = /* GraphQL */ `
  query catsConnectionQuery {
    catsConnection(first: 10) {
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          image
          rate
          wins
          losses
        }
        cursor
      }
      aggregate {
        count
      }
    }
  }
`;

const rateMutation = /* GraphQL */ `
  mutation rateMutation($winner: ID!, $loser: ID!) {
    rate(winner: $winner, loser: $loser) {
      id
      image
      rate
      wins
      losses
    }
  }
`;

// --------------------------------------------------------------------------
const catObject = {
  id: expect.any(String),
  image: expect.any(String),
  rate: expect.any(Number),
  wins: expect.any(Number),
  losses: expect.any(Number),
};

// --------------------------------------------------------------------------
// TESTS
// --------------------------------------------------------------------------
describe("> Cats", (): void => {
  let server: ServerTest;
  let app: App;
  let cat: Cat;
  let loser: Cat;

  beforeAll(
    async (): Promise<void> => {
      ({ server, app } = await createServer());
    },
  );

  afterAll(
    async (): Promise<void> => {
      await app.close();
    },
  );

  test("should return all cats", async (): Promise<void> => {
    await server
      .sendQL(catsQuery)
      .expect(({ body }: request.Response): void => {
        expect(body.data.catsConnection).toBeDefined();
        expect(body.data.catsConnection.aggregate.count).toBeDefined();
        expect(body.data.catsConnection.pageInfo.hasNextPage).toBe(true);
        expect(body.data.catsConnection.edges).toBeInstanceOf(Array);
        expect(body.data.catsConnection.edges.length).toBe(10);
        expect(body.data.catsConnection.edges[0].cursor).toBeDefined();
        expect(body.data.catsConnection.edges[0].node).toBeInstanceOf(Object);

        expect(body.data.catsConnection.edges[0].node).toMatchObject(catObject);

        cat = body.data.catsConnection.edges[0].node;
        loser = body.data.catsConnection.edges[1].node;
      })
      .expect(200);
  });

  test("should return a cat", async (): Promise<void> => {
    await server
      .sendQL(catQuery, {
        where: { id: cat.id },
      })
      .expect(({ body }: request.Response): void => {
        expect(body.data.cat).toMatchObject(cat);
      })
      .expect(200);
  });

  test("should rate 2 cats", async (): Promise<void> => {
    await server
      .sendQL(rateMutation, { winner: cat.id, loser: loser.id })
      .expect(({ body }: request.Response): void => {
        const winnerCat = body.data.rate.find((c: Cat) => c.id === cat.id);
        const loserCat = body.data.rate.find((c: Cat) => c.id === loser.id);
        expect(winnerCat.rate > cat.rate).toBeTruthy();
        expect(loserCat.rate < loser.rate).toBeTruthy();
      })
      .expect(200);
  });

  test("should return 2 ramdom cats", async (): Promise<void> => {
    const catIds: string[] = [];

    await server
      .sendQL(randomQuery)
      .expect(({ body }: request.Response): void => {
        expect(body.data.random[0]).toMatchObject(catObject);
        expect(body.data.random[1]).toMatchObject(catObject);

        catIds.push(body.data.random[0].id);
        catIds.push(body.data.random[1].id);
      })
      .expect(200);

    await server
      .sendQL(randomQuery)
      .expect(({ body }: request.Response): void => {
        expect(catIds).not.toContain([body.data.random[0].id, body.data.random[1].id]);
      })
      .expect(200);
  });
});
