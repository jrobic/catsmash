import { rate } from "./rating";

describe("> Ratings", (): void => {
  const Ra = 1540;
  const Rb = 1500;

  test("when player B win", (): void => {
    expect(rate(Rb, Ra)).toMatchObject({
      Rl: 1522.1664,
      Rw: 1517.8336,
    });
  });

  test("should maintains an absolute rating floor of 100", (): void => {
    expect(rate(140, 100)).toMatchObject({
      Rl: 100,
      Rw: 154.1664,
    });
  });
});
