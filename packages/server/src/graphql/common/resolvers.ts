import { MyContext } from "../../types/Context";
import { Version, name, version } from "../../utils/version";

interface StatusResponse {
  prisma: "ok" | "nok";
}

export default {
  Query: {
    version(): Version {
      return {
        name,
        version,
      };
    },

    async status<Root, Args>(
      _root: Root,
      _args: Args,
      context: MyContext,
    ): Promise<StatusResponse> {
      const cats = await context.prisma.cats();

      return { prisma: cats !== undefined ? "ok" : "nok" };
    },
  },
};
