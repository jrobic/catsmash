// eslint-disable-next-line import/no-extraneous-dependencies
import request from "supertest";
import { createServer, ServerTest } from "../../tests-utils/common";
import { App } from "../../types/Server";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const packageJSON = require("../../../package.json");

const versionQuery = /* GraphQL */ `
  query versionQuery {
    version {
      name
      version
    }
  }
`;

const statusQuery = /* GraphQL */ `
  query statusQuery {
    status {
      prisma
    }
  }
`;
// --------------------------------------------------------------------------
// TESTS
// --------------------------------------------------------------------------
describe("> Common", (): void => {
  let server: ServerTest;
  let app: App;

  beforeAll(
    async (): Promise<void> => {
      ({ server, app } = await createServer());
    },
  );

  afterAll(
    async (): Promise<void> => {
      await app.close();
    },
  );

  test("should return api version", async (): Promise<void> => {
    await server
      .sendQL(versionQuery)
      .expect(({ body }: request.Response): void => {
        expect(body.data.version.name).toEqual(packageJSON.name);
        expect(body.data.version.version).toEqual(packageJSON.version);
      })
      .expect(200);
  });

  test("should return api status", async (): Promise<void> => {
    await server
      .sendQL(statusQuery)
      .expect(({ body }: request.Response): void => {
        expect(body.data.status.prisma).toEqual("ok");
      })
      .expect(200);
  });
});
