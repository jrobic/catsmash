import gql from "graphql-tag";
import * as ApolloReactCommon from "@apollo/react-common";
import * as ApolloReactHooks from "@apollo/react-hooks";
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
  Long: any;
  Json: any;
};

export type AggregateCat = {
  __typename?: "AggregateCat";
  count: Scalars["Int"];
};

export type BatchPayload = {
  __typename?: "BatchPayload";
  count: Scalars["Long"];
};

export type Cat = {
  __typename?: "Cat";
  id: Scalars["ID"];
  createdAt: Scalars["DateTime"];
  updatedAt: Scalars["DateTime"];
  deletedAt?: Maybe<Scalars["DateTime"]>;
  image: Scalars["String"];
  rate: Scalars["Float"];
  rank: Scalars["Int"];
  wins: Scalars["Float"];
  losses: Scalars["Float"];
};

export type CatConnection = {
  __typename?: "CatConnection";
  pageInfo: PageInfo;
  edges: Array<Maybe<CatEdge>>;
  aggregate: AggregateCat;
};

export type CatCreateInput = {
  id?: Maybe<Scalars["ID"]>;
  deletedAt?: Maybe<Scalars["DateTime"]>;
  image: Scalars["String"];
  rate?: Maybe<Scalars["Float"]>;
  rank?: Maybe<Scalars["Int"]>;
  wins?: Maybe<Scalars["Float"]>;
  losses?: Maybe<Scalars["Float"]>;
};

export type CatEdge = {
  __typename?: "CatEdge";
  node: Cat;
  cursor: Scalars["String"];
};

export enum CatOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  DeletedAtAsc = "deletedAt_ASC",
  DeletedAtDesc = "deletedAt_DESC",
  ImageAsc = "image_ASC",
  ImageDesc = "image_DESC",
  RateAsc = "rate_ASC",
  RateDesc = "rate_DESC",
  RankAsc = "rank_ASC",
  RankDesc = "rank_DESC",
  WinsAsc = "wins_ASC",
  WinsDesc = "wins_DESC",
  LossesAsc = "losses_ASC",
  LossesDesc = "losses_DESC",
}

export type CatPreviousValues = {
  __typename?: "CatPreviousValues";
  id: Scalars["ID"];
  createdAt: Scalars["DateTime"];
  updatedAt: Scalars["DateTime"];
  deletedAt?: Maybe<Scalars["DateTime"]>;
  image: Scalars["String"];
  rate: Scalars["Float"];
  rank: Scalars["Int"];
  wins: Scalars["Float"];
  losses: Scalars["Float"];
};

export type CatSubscriptionPayload = {
  __typename?: "CatSubscriptionPayload";
  mutation: MutationType;
  node?: Maybe<Cat>;
  updatedFields?: Maybe<Array<Scalars["String"]>>;
  previousValues?: Maybe<CatPreviousValues>;
};

export type CatSubscriptionWhereInput = {
  mutation_in?: Maybe<Array<MutationType>>;
  updatedFields_contains?: Maybe<Scalars["String"]>;
  updatedFields_contains_every?: Maybe<Array<Scalars["String"]>>;
  updatedFields_contains_some?: Maybe<Array<Scalars["String"]>>;
  node?: Maybe<CatWhereInput>;
  AND?: Maybe<Array<CatSubscriptionWhereInput>>;
  OR?: Maybe<Array<CatSubscriptionWhereInput>>;
  NOT?: Maybe<Array<CatSubscriptionWhereInput>>;
};

export type CatUpdateInput = {
  deletedAt?: Maybe<Scalars["DateTime"]>;
  image?: Maybe<Scalars["String"]>;
  rate?: Maybe<Scalars["Float"]>;
  rank?: Maybe<Scalars["Int"]>;
  wins?: Maybe<Scalars["Float"]>;
  losses?: Maybe<Scalars["Float"]>;
};

export type CatUpdateManyMutationInput = {
  deletedAt?: Maybe<Scalars["DateTime"]>;
  image?: Maybe<Scalars["String"]>;
  rate?: Maybe<Scalars["Float"]>;
  rank?: Maybe<Scalars["Int"]>;
  wins?: Maybe<Scalars["Float"]>;
  losses?: Maybe<Scalars["Float"]>;
};

export type CatWhereInput = {
  id?: Maybe<Scalars["ID"]>;
  id_not?: Maybe<Scalars["ID"]>;
  id_in?: Maybe<Array<Scalars["ID"]>>;
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  id_lt?: Maybe<Scalars["ID"]>;
  id_lte?: Maybe<Scalars["ID"]>;
  id_gt?: Maybe<Scalars["ID"]>;
  id_gte?: Maybe<Scalars["ID"]>;
  id_contains?: Maybe<Scalars["ID"]>;
  id_not_contains?: Maybe<Scalars["ID"]>;
  id_starts_with?: Maybe<Scalars["ID"]>;
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  id_ends_with?: Maybe<Scalars["ID"]>;
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  deletedAt?: Maybe<Scalars["DateTime"]>;
  deletedAt_not?: Maybe<Scalars["DateTime"]>;
  deletedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  deletedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  deletedAt_lt?: Maybe<Scalars["DateTime"]>;
  deletedAt_lte?: Maybe<Scalars["DateTime"]>;
  deletedAt_gt?: Maybe<Scalars["DateTime"]>;
  deletedAt_gte?: Maybe<Scalars["DateTime"]>;
  image?: Maybe<Scalars["String"]>;
  image_not?: Maybe<Scalars["String"]>;
  image_in?: Maybe<Array<Scalars["String"]>>;
  image_not_in?: Maybe<Array<Scalars["String"]>>;
  image_lt?: Maybe<Scalars["String"]>;
  image_lte?: Maybe<Scalars["String"]>;
  image_gt?: Maybe<Scalars["String"]>;
  image_gte?: Maybe<Scalars["String"]>;
  image_contains?: Maybe<Scalars["String"]>;
  image_not_contains?: Maybe<Scalars["String"]>;
  image_starts_with?: Maybe<Scalars["String"]>;
  image_not_starts_with?: Maybe<Scalars["String"]>;
  image_ends_with?: Maybe<Scalars["String"]>;
  image_not_ends_with?: Maybe<Scalars["String"]>;
  rate?: Maybe<Scalars["Float"]>;
  rate_not?: Maybe<Scalars["Float"]>;
  rate_in?: Maybe<Array<Scalars["Float"]>>;
  rate_not_in?: Maybe<Array<Scalars["Float"]>>;
  rate_lt?: Maybe<Scalars["Float"]>;
  rate_lte?: Maybe<Scalars["Float"]>;
  rate_gt?: Maybe<Scalars["Float"]>;
  rate_gte?: Maybe<Scalars["Float"]>;
  rank?: Maybe<Scalars["Int"]>;
  rank_not?: Maybe<Scalars["Int"]>;
  rank_in?: Maybe<Array<Scalars["Int"]>>;
  rank_not_in?: Maybe<Array<Scalars["Int"]>>;
  rank_lt?: Maybe<Scalars["Int"]>;
  rank_lte?: Maybe<Scalars["Int"]>;
  rank_gt?: Maybe<Scalars["Int"]>;
  rank_gte?: Maybe<Scalars["Int"]>;
  wins?: Maybe<Scalars["Float"]>;
  wins_not?: Maybe<Scalars["Float"]>;
  wins_in?: Maybe<Array<Scalars["Float"]>>;
  wins_not_in?: Maybe<Array<Scalars["Float"]>>;
  wins_lt?: Maybe<Scalars["Float"]>;
  wins_lte?: Maybe<Scalars["Float"]>;
  wins_gt?: Maybe<Scalars["Float"]>;
  wins_gte?: Maybe<Scalars["Float"]>;
  losses?: Maybe<Scalars["Float"]>;
  losses_not?: Maybe<Scalars["Float"]>;
  losses_in?: Maybe<Array<Scalars["Float"]>>;
  losses_not_in?: Maybe<Array<Scalars["Float"]>>;
  losses_lt?: Maybe<Scalars["Float"]>;
  losses_lte?: Maybe<Scalars["Float"]>;
  losses_gt?: Maybe<Scalars["Float"]>;
  losses_gte?: Maybe<Scalars["Float"]>;
  AND?: Maybe<Array<CatWhereInput>>;
  OR?: Maybe<Array<CatWhereInput>>;
  NOT?: Maybe<Array<CatWhereInput>>;
};

export type CatWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
  image?: Maybe<Scalars["String"]>;
};

export type Mutation = {
  __typename?: "Mutation";
  createCat: Cat;
  updateCat?: Maybe<Cat>;
  updateManyCats: BatchPayload;
  upsertCat: Cat;
  deleteCat?: Maybe<Cat>;
  deleteManyCats: BatchPayload;
  rate: Array<Maybe<Cat>>;
};

export type MutationCreateCatArgs = {
  data: CatCreateInput;
};

export type MutationUpdateCatArgs = {
  data: CatUpdateInput;
  where: CatWhereUniqueInput;
};

export type MutationUpdateManyCatsArgs = {
  data: CatUpdateManyMutationInput;
  where?: Maybe<CatWhereInput>;
};

export type MutationUpsertCatArgs = {
  where: CatWhereUniqueInput;
  create: CatCreateInput;
  update: CatUpdateInput;
};

export type MutationDeleteCatArgs = {
  where: CatWhereUniqueInput;
};

export type MutationDeleteManyCatsArgs = {
  where?: Maybe<CatWhereInput>;
};

export type MutationRateArgs = {
  winner: Scalars["ID"];
  loser: Scalars["ID"];
};

export enum MutationType {
  Created = "CREATED",
  Updated = "UPDATED",
  Deleted = "DELETED",
}

export type Node = {
  id: Scalars["ID"];
};

export type PageInfo = {
  __typename?: "PageInfo";
  hasNextPage: Scalars["Boolean"];
  hasPreviousPage: Scalars["Boolean"];
  startCursor?: Maybe<Scalars["String"]>;
  endCursor?: Maybe<Scalars["String"]>;
};

export enum PrismaStatus {
  Ok = "ok",
  Nok = "nok",
}

export type Query = {
  __typename?: "Query";
  cat?: Maybe<Cat>;
  cats: Array<Maybe<Cat>>;
  catsConnection: CatConnection;
  node?: Maybe<Node>;
  random: Array<Maybe<Cat>>;
  status: Status;
  version: Version;
};

export type QueryCatArgs = {
  where: CatWhereUniqueInput;
};

export type QueryCatsArgs = {
  where?: Maybe<CatWhereInput>;
  orderBy?: Maybe<CatOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
};

export type QueryCatsConnectionArgs = {
  where?: Maybe<CatWhereInput>;
  orderBy?: Maybe<CatOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
};

export type QueryNodeArgs = {
  id: Scalars["ID"];
};

export type Status = {
  __typename?: "Status";
  prisma: PrismaStatus;
};

export type Subscription = {
  __typename?: "Subscription";
  cat?: Maybe<CatSubscriptionPayload>;
};

export type SubscriptionCatArgs = {
  where?: Maybe<CatSubscriptionWhereInput>;
};

export type Version = {
  __typename?: "Version";
  name: Scalars["String"];
  version: Scalars["String"];
};

export type CatFieldsFragment = { __typename?: "Cat" } & Pick<
  Cat,
  "id" | "image" | "rank" | "rate" | "wins" | "losses"
>;

export type RandomCatsQueryVariables = {};

export type RandomCatsQuery = { __typename?: "Query" } & {
  random: Array<Maybe<{ __typename?: "Cat" } & CatFieldsFragment>>;
};

export type RateCatsMutationVariables = {
  winner: Scalars["ID"];
  loser: Scalars["ID"];
};

export type RateCatsMutation = { __typename?: "Mutation" } & {
  rate: Array<Maybe<{ __typename?: "Cat" } & CatFieldsFragment>>;
};

export type CatsConnectionQueryVariables = {
  where?: Maybe<CatWhereInput>;
  orderBy?: Maybe<CatOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
};

export type CatsConnectionQuery = { __typename?: "Query" } & {
  catsConnection: { __typename?: "CatConnection" } & {
    pageInfo: { __typename?: "PageInfo" } & Pick<
      PageInfo,
      "hasNextPage" | "hasPreviousPage" | "startCursor" | "endCursor"
    >;
    edges: Array<
      Maybe<
        { __typename?: "CatEdge" } & Pick<CatEdge, "cursor"> & {
            node: { __typename?: "Cat" } & CatFieldsFragment;
          }
      >
    >;
    aggregate: { __typename?: "AggregateCat" } & Pick<AggregateCat, "count">;
  };
};

export const CatFieldsFragmentDoc = gql`
  fragment CatFields on Cat {
    id
    image
    rank
    rate
    wins
    losses
  }
`;
export const RandomCatsDocument = gql`
  query randomCats {
    random {
      ...CatFields
    }
  }
  ${CatFieldsFragmentDoc}
`;

/**
 * __useRandomCatsQuery__
 *
 * To run a query within a React component, call `useRandomCatsQuery` and pass it any options that fit your needs.
 * When your component renders, `useRandomCatsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRandomCatsQuery({
 *   variables: {
 *   },
 * });
 */
export function useRandomCatsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<RandomCatsQuery, RandomCatsQueryVariables>,
) {
  return ApolloReactHooks.useQuery<RandomCatsQuery, RandomCatsQueryVariables>(
    RandomCatsDocument,
    baseOptions,
  );
}
export function useRandomCatsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<RandomCatsQuery, RandomCatsQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<RandomCatsQuery, RandomCatsQueryVariables>(
    RandomCatsDocument,
    baseOptions,
  );
}
export type RandomCatsQueryHookResult = ReturnType<typeof useRandomCatsQuery>;
export type RandomCatsLazyQueryHookResult = ReturnType<typeof useRandomCatsLazyQuery>;
export type RandomCatsQueryResult = ApolloReactCommon.QueryResult<
  RandomCatsQuery,
  RandomCatsQueryVariables
>;
export const RateCatsDocument = gql`
  mutation rateCats($winner: ID!, $loser: ID!) {
    rate(winner: $winner, loser: $loser) {
      ...CatFields
    }
  }
  ${CatFieldsFragmentDoc}
`;
export type RateCatsMutationFn = ApolloReactCommon.MutationFunction<
  RateCatsMutation,
  RateCatsMutationVariables
>;

/**
 * __useRateCatsMutation__
 *
 * To run a mutation, you first call `useRateCatsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRateCatsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [rateCatsMutation, { data, loading, error }] = useRateCatsMutation({
 *   variables: {
 *      winner: // value for 'winner'
 *      loser: // value for 'loser'
 *   },
 * });
 */
export function useRateCatsMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<RateCatsMutation, RateCatsMutationVariables>,
) {
  return ApolloReactHooks.useMutation<RateCatsMutation, RateCatsMutationVariables>(
    RateCatsDocument,
    baseOptions,
  );
}
export type RateCatsMutationHookResult = ReturnType<typeof useRateCatsMutation>;
export type RateCatsMutationResult = ApolloReactCommon.MutationResult<RateCatsMutation>;
export type RateCatsMutationOptions = ApolloReactCommon.BaseMutationOptions<
  RateCatsMutation,
  RateCatsMutationVariables
>;
export const CatsConnectionDocument = gql`
  query catsConnection(
    $where: CatWhereInput
    $orderBy: CatOrderByInput
    $skip: Int
    $after: String
    $before: String
    $first: Int
    $last: Int
  ) {
    catsConnection(
      where: $where
      orderBy: $orderBy
      skip: $skip
      after: $after
      before: $before
      first: $first
      last: $last
    ) {
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          ...CatFields
        }
        cursor
      }
      aggregate {
        count
      }
    }
  }
  ${CatFieldsFragmentDoc}
`;

/**
 * __useCatsConnectionQuery__
 *
 * To run a query within a React component, call `useCatsConnectionQuery` and pass it any options that fit your needs.
 * When your component renders, `useCatsConnectionQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCatsConnectionQuery({
 *   variables: {
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *      skip: // value for 'skip'
 *      after: // value for 'after'
 *      before: // value for 'before'
 *      first: // value for 'first'
 *      last: // value for 'last'
 *   },
 * });
 */
export function useCatsConnectionQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    CatsConnectionQuery,
    CatsConnectionQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<CatsConnectionQuery, CatsConnectionQueryVariables>(
    CatsConnectionDocument,
    baseOptions,
  );
}
export function useCatsConnectionLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    CatsConnectionQuery,
    CatsConnectionQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<CatsConnectionQuery, CatsConnectionQueryVariables>(
    CatsConnectionDocument,
    baseOptions,
  );
}
export type CatsConnectionQueryHookResult = ReturnType<typeof useCatsConnectionQuery>;
export type CatsConnectionLazyQueryHookResult = ReturnType<typeof useCatsConnectionLazyQuery>;
export type CatsConnectionQueryResult = ApolloReactCommon.QueryResult<
  CatsConnectionQuery,
  CatsConnectionQueryVariables
>;
