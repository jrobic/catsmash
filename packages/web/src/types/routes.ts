import { RouteProps } from "react-router-dom";

export interface MyRouteProps extends RouteProps {
  name: string;
}
