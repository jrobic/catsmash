import { ApolloClient, HttpLink, ApolloLink, InMemoryCache } from "apollo-boost";
import apolloLogger from "apollo-link-logger";

const httpLink = new HttpLink({
  uri: `${process.env.REACT_APP_GRAPHQL_ENDPOINT}`,
});

const link = ApolloLink.from(
  [process.env.NODE_ENV !== "production" ? apolloLogger : null, httpLink].filter(Boolean),
);

export const client = new ApolloClient({ link, cache: new InMemoryCache() });
