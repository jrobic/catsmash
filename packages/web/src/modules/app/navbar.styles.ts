import { css } from "@emotion/core";
import { colors, navbarSize, breakPoints } from "../../themes";

export const header = css``;
export const nav = css`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  position: fixed;
  top: 0;

  width: 100%;
  z-index: 100;
  background-color: ${colors.background.default};
  height: ${navbarSize.height};
  border-bottom: 1px solid #eee;

  padding: 0 3rem;

  transition: all 200ms ease;
  z-index: 100;
`;

export const brand = css`
  display: flex;
  align-items: center;
  justify-content: center;
  text-decoration: none;
  color: ${colors.dracula};
  font-weight: 700;
  font-size: 1.2rem;
`;

export const mainNav = css`
  padding: 0 1rem;
  display: flex;
  align-items: center;

  @media (min-width: ${breakPoints.tab}px) {
    flex: 1;
  }

  a {
    display: block;
    cursor: pointer;
    font-weight: 500;
    color: ${colors.greenish};
    text-decoration: none;
    transition: all 200ms ease;

    &:hover,
    &:focus,
    &.active {
      color: ${colors.mint};
    }
  }
`;
