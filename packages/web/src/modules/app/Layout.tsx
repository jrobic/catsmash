import React from "react";
import { Switch, Redirect, Route } from "react-router-dom";

import { MyRouteProps } from "../../types/routes";
import { routes } from "./routes";
import { Navbar } from "./Navbar";
import * as styles from "./layout.styles";

const renderRoute: React.FC<MyRouteProps> = route => {
  return (
    <Route key={route.name} path={route.path} exact={route.exact} component={route.component} />
  );
};

export const Layout: React.FC = () => {
  return (
    <div>
      <Navbar />
      <div css={styles.content}>
        <Switch>
          {routes.map(renderRoute)}
          <Redirect to="/" />
        </Switch>
      </div>
    </div>
  );
};
