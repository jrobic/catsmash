import React from "react";
import { render } from "@testing-library/react";
import { withRouter } from "../../helpers/test-utils";
import { Navbar } from "./Navbar";

describe("Navbar", () => {
  it("should render correctly", () => {
    const { Component } = withRouter(<Navbar />);
    const { asFragment } = render(Component);

    expect(asFragment()).toMatchSnapshot();
  });

  it("should have root link", () => {
    const { Component } = withRouter(<Navbar />);
    const { getByTestId } = render(Component);

    expect(getByTestId("root-link")).toBeInTheDocument();
  });

  it("should have ranking link", () => {
    const { Component } = withRouter(<Navbar />);
    const { getByTestId } = render(Component);

    expect(getByTestId("ranking-link")).toBeInTheDocument();
  });
});
