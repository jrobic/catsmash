import { css } from "@emotion/core";
import { navbarSize } from "../../themes";

export const content = (): any => css`
  min-height: 100vh;
  padding-top: ${navbarSize.height};
`;
