import { MyRouteProps } from "../../types/routes";
import { Battle } from "../battle";
import { Ranking } from "../ranking";

export const routes: MyRouteProps[] = [
  {
    path: "/",
    exact: true,
    name: "battle",
    component: Battle,
  },
  {
    path: "/ranking",
    name: "ranking",
    component: Ranking,
  },
];
