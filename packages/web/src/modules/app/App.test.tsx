import React from "react";
import { render } from "@testing-library/react";
import { withRouter } from "../../helpers/test-utils";
import App from "./App";

test("App - default route", () => {
  const { Component } = withRouter(<App />);
  const { getByTestId } = render(Component);
  const battleEl = getByTestId("battle");
  expect(battleEl).toBeInTheDocument();
});
