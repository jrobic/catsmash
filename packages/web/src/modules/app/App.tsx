import React from "react";

import { BrowserRouter as Router } from "react-router-dom";
import { ApolloProvider } from "@apollo/react-hooks";

import { GlobalTheme } from "../../themes";
import { Layout } from "./Layout";
import { client } from "./apollo";

const App: React.FC = () => {
  return (
    <ApolloProvider client={client}>
      <Router>
        <GlobalTheme />
        <Layout />
      </Router>
    </ApolloProvider>
  );
};

export default App;
