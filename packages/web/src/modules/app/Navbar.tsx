import React from "react";
import { Link } from "react-router-dom";

import * as styles from "./navbar.styles";

export const Navbar: React.FC = () => {
  return (
    <header css={styles.header}>
      <nav css={styles.nav}>
        <Link data-testid="root-link" to="/" css={styles.brand}>
          <span>CATSMASH</span>
        </Link>
        <div css={styles.mainNav}>
          <Link data-testid="ranking-link" to="/ranking">
            ranking
          </Link>
        </div>
      </nav>
    </header>
  );
};
