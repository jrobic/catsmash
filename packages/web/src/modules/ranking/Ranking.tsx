import React from "react";
import { useCatsConnectionQuery, CatOrderByInput, CatEdge } from "../../generated/graphql";
import { RankingItem } from "./RankingItem";

import * as styles from "./ranking.styles";

export const Ranking: React.FC = () => {
  const { data } = useCatsConnectionQuery({
    variables: { orderBy: CatOrderByInput.RankDesc, first: 100 },
    fetchPolicy: "cache-and-network",
    pollInterval: 15 * 1000,
  });

  return (
    <div data-testid="ranking" css={styles.content}>
      <div css={styles.list}>
        {((data?.catsConnection?.edges || []) as CatEdge[]).map(RankingItem)}
      </div>
    </div>
  );
};
