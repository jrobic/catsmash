import React from "react";
import { render, act, wait } from "@testing-library/react";
import { MockedProvider } from "@apollo/react-testing";

import { Ranking } from "./Ranking";
import { CatsConnectionDocument, CatOrderByInput } from "../../generated/graphql";

const mocks = [
  {
    request: {
      query: CatsConnectionDocument,
      variables: { orderBy: CatOrderByInput.RankDesc, first: 100 },
    },
    result: {
      data: {
        catsConnection: {
          pageInfo: {
            hasNextPage: true,
            hasPreviousPage: false,
            startCursor: "ck66wxavq060s0729mltobvru",
            endCursor: "ck66wxawa062a0729o94g52y7",
            __typename: "PageInfo",
          },
          edges: [
            {
              node: {
                id: "ck66wxavq060s0729mltobvru",
                image: "http://25.media.tumblr.com/tumblr_m1mls6SBfO1qze0hyo1_1280.jpg",
                rank: 1546,
                rate: 1546.56,
                wins: 3,
                losses: 0,
                __typename: "Cat",
              },
              cursor: "ck66wxavq060s0729mltobvru",
              __typename: "CatEdge",
            },
            {
              node: {
                id: "ck66wxaw4061s0729ojaovma3",
                image: "http://24.media.tumblr.com/tumblr_lg1hnknXjm1qfyzelo1_1280.jpg",
                rank: 1546,
                rate: 1545.8272,
                wins: 3,
                losses: 0,
                __typename: "Cat",
              },
              cursor: "ck66wxaw4061s0729ojaovma3",
              __typename: "CatEdge",
            },
            {
              node: {
                id: "ck66wxawa062a0729o94g52y7",
                image: "http://25.media.tumblr.com/tumblr_m4pwa9EXE41r6jd7fo1_500.jpg",
                rank: 1545,
                rate: 1545.1296,
                wins: 3,
                losses: 0,
                __typename: "Cat",
              },
              cursor: "ck66wxawa062a0729o94g52y7",
              __typename: "CatEdge",
            },
          ],
          aggregate: { count: 3, __typename: "AggregateCat" },
          __typename: "CatConnection",
        },
      },
    },
  },
];

describe("Ranking", () => {
  it("default render", async () => {
    await act(async () => {
      const { asFragment, findAllByTestId } = render(
        <MockedProvider mocks={mocks}>
          <Ranking />
        </MockedProvider>,
      );

      await wait(
        async (): Promise<void> => {
          await findAllByTestId(/ranking-list-item/);
        },
      );

      expect(asFragment()).toMatchSnapshot();
    });
  });
});
