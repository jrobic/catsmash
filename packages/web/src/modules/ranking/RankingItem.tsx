import React from "react";
import { CatEdge } from "../../generated/graphql";

import * as styles from "./ranking.styles";

export const RankingItem: React.FC<CatEdge> = ({ node }) => {
  return (
    <div key={node.id} css={styles.listItem} data-testid={`ranking-list-item-${node.id}`}>
      <div css={styles.listItemHeader}>
        <span>Rank:</span>
        <span>{node.rank}</span>
      </div>
      <div css={styles.listItemImg(node.image)} />
      <div css={styles.listItemContent}>
        <div>
          <span>Score:</span>
          <span>{node.rate}</span>
        </div>
        <div>
          <span>Wins:</span>
          <span>{node.wins}</span>
        </div>
        <div>
          <span>Losses:</span>
          <span>{node.losses}</span>
        </div>
      </div>
    </div>
  );
};
