import { css } from "@emotion/core";
import { colors, breakPoints } from "../../themes";

export const content = (): any => css``;

export const list = (): any => css`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const listItem = (): any => css`
  display: flex;
  flex-direction: column;

  max-width: 200px;
  width: 100%;
  overflow: hidden;

  margin: 0.5rem 0;
  background-color: #eff3f5;
  border-radius: 4px;
  border: 2px solid transparent;

  @media (min-width: ${breakPoints.tab}px) {
    max-width: 275px;
  }

  @media (min-width: ${breakPoints.largeTab}px) {
    max-width: 345px;
  }

  &:hover,
  &:focus {
    border-color: ${colors.mint};
  }
`;

export const listItemImg = (image: string): any => css`
  height: 0;
  padding-top: 56.25%;
  display: block;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  background-image: url(${image});
`;

export const listItemContent = (): any => css`
  padding: 1rem;

  > div {
    line-height: 1.3rem;

    > span {
      font-size: 0.85rem;
      font-weight: 500;

      &:last-child {
        margin-left: 0.3rem;
        color: ${colors.mint};
      }
    }
  }
`;

export const listItemHeader = (): any => css`
  display: flex;
  padding: 1rem;
  align-items: center;
  justify-content: space-between;

  font-size: 0.9rem;
  font-weight: 700;

  span {
    line-height: 1.1rem;
  }
  span:last-child {
    font-size: 1.1rem;
    color: ${colors.mint};
  }
`;
