import React from "react";
import { render, waitForElement } from "@testing-library/react";
import { MockedProvider } from "@apollo/react-testing";
import { Battle } from "./Battle";
import { RandomCatsDocument } from "../../generated/graphql";

const mocks = [
  {
    request: {
      query: RandomCatsDocument,
    },
    result: {
      data: {
        random: [
          {
            id: "ck66wxavv06110729p0oqgoqx",
            image: "http://25.media.tumblr.com/tumblr_m4rorb74ne1qkk65ko1_1280.jpg",
            rank: 1469,
            rate: 1469.4688,
            wins: 0,
            losses: 2,
            __typename: "Cat",
          },
          {
            id: "ck66wxav805zi0729w77ga4b8",
            image: "http://25.media.tumblr.com/tumblr_m20bftDngo1qejbiro1_1280.jpg",
            rank: 0,
            rate: 1500,
            wins: 0,
            losses: 0,
            __typename: "Cat",
          },
        ],
      },
    },
  },
];

describe("Battle", () => {
  it("default render", async () => {
    const { asFragment, getByTestId } = render(
      <MockedProvider mocks={mocks}>
        <Battle />
      </MockedProvider>,
    );

    await waitForElement(() => getByTestId("battle-box-ck66wxavv06110729p0oqgoqx"));

    expect(asFragment()).toMatchSnapshot();
  });
});
