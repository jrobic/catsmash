import { css } from "@emotion/core";
import { breakPoints } from "../../themes";

export const layout = css`
  display: flex;
  flex-direction: column;
  min-height: calc(100vh - 3rem);

  @media (min-width: ${breakPoints.largeTab}px) {
    flex-direction: row;
  }
`;
