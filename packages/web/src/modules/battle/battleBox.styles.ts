import { css } from "@emotion/core";
import { colors, breakPoints } from "../../themes";

export const box = (type: "even" | "odd"): any => css`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;

  background-color: ${type === "even" ? "#eff3f5" : "#fff"};

  > div {
    cursor: pointer;
    overflow: hidden;
    width: 200px;
    height: 200px;
    border-radius: 50%;
    border: 4px solid ${type === "odd" ? "#eff3f5" : "#fff"};
    outline: none;

    @media (min-width: ${breakPoints.tab}px) {
      width: 275px;
      height: 275px;
    }

    @media (min-width: ${breakPoints.largeTab}px) {
      width: 350px;
      height: 350px;
    }

    &:hover {
      border-color: ${colors.greenish};

      img {
        opacity: 1;
      }
    }

    img {
      min-height: 100%;
      max-width: 100%;
      width: auto;
      opacity: 0.93;
      transition: opacity 0.6s ease 0s, border-color 0.3s ease 0s;
    }
  }
`;
