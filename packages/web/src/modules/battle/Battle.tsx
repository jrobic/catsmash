import React from "react";

import * as styles from "./battle.styles";
import { BattleBox } from "./BattleBox";
import { useRandomCatsQuery, Cat, useRateCatsMutation } from "../../generated/graphql";

export const Battle: React.FC = () => {
  const { data, refetch } = useRandomCatsQuery();
  const [rateMutation] = useRateCatsMutation();

  const onSelect = async (winnerCat: Cat): Promise<void> => {
    const loser = data?.random.find(cat => cat?.id !== winnerCat.id) as Cat;

    await rateMutation({ variables: { winner: winnerCat.id, loser: loser.id } });
    await refetch();
  };

  return (
    <div data-testid="battle" css={styles.layout}>
      <BattleBox cat={data?.random[0] as Cat} onSelect={onSelect} />
      <BattleBox cat={data?.random[1] as Cat} onSelect={onSelect} type="odd" />
    </div>
  );
};
