import React, { useCallback } from "react";

import * as styles from "./battleBox.styles";
import { Cat, Maybe } from "../../generated/graphql";

interface BattleBoxProps {
  cat: Maybe<Cat>;
  type?: "even" | "odd";
  onSelect?(cat: Cat): void;
}

export const BattleBox: React.FC<BattleBoxProps> = ({ cat, type = "even", onSelect }) => {
  const onSelectCb = useCallback(() => {
    if (cat && onSelect) {
      onSelect(cat);
    }
  }, [cat, onSelect]);

  return (
    <div css={styles.box(type)} data-testid={`battle-box-${cat?.id}`}>
      <div onClick={onSelectCb} onKeyPress={onSelectCb} role="button" tabIndex={0}>
        {cat?.image && <img src={cat?.image} alt="A cat" />}
      </div>
    </div>
  );
};
