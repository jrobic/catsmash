import React from "react";
import { render } from "@testing-library/react";
import { Cat } from "../../generated/graphql";
import { BattleBox } from "./BattleBox";

const cat: Cat = {
  image: "http://24.media.tumblr.com/tumblr_m82woaL5AD1rro1o5o1_1280.jpg",
  id: "1234",
  rank: 1200,
  rate: 1200,
  wins: 2,
  losses: 3,
  createdAt: Date.now(),
  updatedAt: Date.now(),
};

describe("BattleBox", () => {
  it("default render", () => {
    const { asFragment } = render(<BattleBox cat={cat} />);
    expect(asFragment()).toMatchSnapshot();
  });

  it("when type is odd", () => {
    const { asFragment } = render(<BattleBox cat={cat} type="odd" />);
    expect(asFragment()).toMatchSnapshot();
  });
});
