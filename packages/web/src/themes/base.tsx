import React from "react";
import emotionReset from "emotion-reset";
import { Global, css } from "@emotion/core";

import { colors, breakPoints } from "./theme";

const theme = css`
  ${emotionReset}

  *, *::after, *::before {
    box-sizing: border-box;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    font-smoothing: antialiased;
  }

  * {
    box-sizing: border-box;
  }

  html,
  body {
    margin: 0;
    padding: 0;
    min-height: 100vh;
    width: 100%;
    background: ${colors.white};
    color: ${colors.dark};

    line-height: 1.5em;
    font-family: "Open Sans", "Helvetica Neue", sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    font-size: 14px;

    @media (min-width: ${breakPoints.tab}px) {
      font-size: 16px;
    }

    @media (min-width: ${breakPoints.desktop}px) {
      font-size: 18px;
    }
  }
  body {
    width: 100%;
    min-height: 100%;
  }
`;

export const GlobalTheme: React.FC = () => <Global styles={theme} />;
