import React from "react";
import { Router } from "react-router-dom";
import { createMemoryHistory, MemoryHistory } from "history";

export function withRouter(
  ui: any,
  { route = "/" } = {},
): { Component: any; history: MemoryHistory } {
  const history = createMemoryHistory({ initialEntries: [route] });

  return {
    Component: <Router history={history}>{ui}</Router>,
    history,
  };
}
