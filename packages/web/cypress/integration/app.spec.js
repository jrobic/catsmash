describe("Catsmash App", () => {
  it(".should() - display battle page", () => {
    cy.visit("/");

    cy.findByTestId("battle");
  });
  it(".should() - display ranking page", () => {
    cy.visit("/ranking");

    cy.findByTestId("ranking");
  });
});
