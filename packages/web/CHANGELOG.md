# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.4.0](https://gitlab.com/jrobic/catsmash/compare/v0.3.0...v0.4.0) (2020-02-05)


### Features

* **web:** make the app responsive ([8957e04](https://gitlab.com/jrobic/catsmash/commit/8957e04b2ce5939971e8bac36813bb1d0dc8de2d))





# [0.3.0](https://gitlab.com/jrobic/catsmash/compare/v0.2.0...v0.3.0) (2020-02-04)


### Features

* ranking ([45d6a15](https://gitlab.com/jrobic/catsmash/commit/45d6a15663a5c6e70d33136dc7c1957c5b41006f))





# [0.2.0](https://gitlab.com/jrobic/catsmash/compare/v0.1.0...v0.2.0) (2020-02-04)


### Features

* **battle:** create Battle ([f5d3210](https://gitlab.com/jrobic/catsmash/commit/f5d3210bf94183d59f86d7e2df48972409322b6a))
* **layout:** add base theme and init navbar ([020311c](https://gitlab.com/jrobic/catsmash/commit/020311c671f3086af7b6661d5d5caa9b0be01c42))
* **ranking:** add ranking list ([124d72f](https://gitlab.com/jrobic/catsmash/commit/124d72f4bb0261ccb99fcba842cd5b50f563574f))
* **web:** init react router w/ test ([4d8b8e6](https://gitlab.com/jrobic/catsmash/commit/4d8b8e60048353efe219a051537c3c3b5ee6bf09))
