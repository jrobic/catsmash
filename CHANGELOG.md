# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.4.0](https://gitlab.com/jrobic/catsmash/compare/v0.3.0...v0.4.0) (2020-02-05)


### Features

* **web:** make the app responsive ([8957e04](https://gitlab.com/jrobic/catsmash/commit/8957e04b2ce5939971e8bac36813bb1d0dc8de2d))





# [0.3.0](https://gitlab.com/jrobic/catsmash/compare/v0.2.0...v0.3.0) (2020-02-04)


### Features

* ranking ([45d6a15](https://gitlab.com/jrobic/catsmash/commit/45d6a15663a5c6e70d33136dc7c1957c5b41006f))





# [0.2.0](https://gitlab.com/jrobic/catsmash/compare/v0.1.0...v0.2.0) (2020-02-04)


### Bug Fixes

* **prisma:** use mutli staged correctly ([5eb7add](https://gitlab.com/jrobic/catsmash/commit/5eb7add2fd0a54636c19c416c57df846ff74af3a))


### Features

* **battle:** create Battle ([f5d3210](https://gitlab.com/jrobic/catsmash/commit/f5d3210bf94183d59f86d7e2df48972409322b6a))
* **layout:** add base theme and init navbar ([020311c](https://gitlab.com/jrobic/catsmash/commit/020311c671f3086af7b6661d5d5caa9b0be01c42))
* **ranking:** add ranking list ([124d72f](https://gitlab.com/jrobic/catsmash/commit/124d72f4bb0261ccb99fcba842cd5b50f563574f))
* **rating:** add rank ([3ae6b08](https://gitlab.com/jrobic/catsmash/commit/3ae6b08ecb1a6b66a7d40a86fc270cb25b1cf78b))
* **rating:** rating system w/ generate random cat ([6f10077](https://gitlab.com/jrobic/catsmash/commit/6f10077cd51a28218b8e12c9a4c996be6ff767c5))
* **web:** init react router w/ test ([4d8b8e6](https://gitlab.com/jrobic/catsmash/commit/4d8b8e60048353efe219a051537c3c3b5ee6bf09))





# [0.1.0](https://gitlab.com/jrobic/catsmash/compare/v0.0.2...v0.1.0) (2020-02-02)


### Features

* **server:** initialize server w/ graphql, prisma ([52defd8](https://gitlab.com/jrobic/catsmash/commit/52defd86b41de03580e36ecbb5f634e2e2beb338))





## 0.0.2 (2020-02-02)

**Note:** Version bump only for package catsmash
